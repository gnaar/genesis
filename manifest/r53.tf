# https://www.terraform.io/docs/providers/aws/r/route53_zone.html

resource "aws_route53_zone" "root" {
  name    = "${var.domain}"
}
