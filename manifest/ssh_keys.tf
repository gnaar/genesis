resource "aws_key_pair" "ec2_kube_key" {
  key_name   = "ec2_kube_key"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}
